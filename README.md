# arch_PRIME

# In future i will create autoinstall script

How-to:
```
cd ~/
git clone https://github.com/MegaRak64/arch_PRIME.git
sudo -s
cd arch_PRIME
chown root:root ./*
cp -rf prime_* /usr/bin/
mkdir /etc/X11/mhwd.d/
cp -rf *.conf /etc/X11/mhwd.d/
```
# Modify display manager start script
!SDDM:
sudo nano /usr/share/sddm/scripts/Xsetup
```
xrandr --dpi 96
xrandr --setprovideroutputsource modesetting NVIDIA-0
xrandr --auto
```
!LightDM:
sudo nano /etc/lightdm/display_setup.sh
```
xrandr --setprovideroutputsource modesetting NVIDIA-0
xrandr --auto
```
Make exucutable
chmod +x /etc/lightdm/display_setup.sh
Change [SeatDefaults] section in  /etc/lightdm/lightdm.conf:
```
sudo nano /etc/lightdm/lightdm.conf
```
```
[SeatDefaults]
display-setup-script=/etc/lightdm/display_setup.sh
```

# Modify /etc/systemd/system/display-manager.service
Example for lightdm
Before:
```
[Unit]
Description=Light Display Manager
Documentation=man:lightdm(1)
Conflicts=getty@tty1.service
After=getty@tty1.service systemd-user-sessions.service plymouth-quit.service acpid.service

[Service]
ExecStart=/usr/bin/lightdm
Restart=always
IgnoreSIGPIPE=no
BusName=org.freedesktop.DisplayManager

[Install]
Alias=display-manager.service
```
After:
```
[Unit]
Description=Light Display Manager
Documentation=man:lightdm(1)
Conflicts=getty@tty1.service
After=getty@tty1.service systemd-user-sessions.service plymouth-quit.service acpid.service

[Service]
###
ExecStartPre=/bin/bash /bin/prime_checker
###
ExecStart=/usr/bin/lightdm
Restart=always
IgnoreSIGPIPE=no
BusName=org.freedesktop.DisplayManager

[Install]
Alias=display-manager.service
```

  
